# NodeJS / Express Website Taxonomy API
Single endpoint API built with NodeJs and Express which accepts a valid Wesbite url, fetches from the IMB Watson API (Natural Language Endpoint) the url categorisation and outputs the result as JSON object.

## Installation
npm install

## Build & Run + Watcher
npm run build; npm run dev-start

## Run Tests
npm test

## Curl endpoint
curl http://localhost:3005/api/watson-nlu/[websiteURL]

## jsDoc
./node_modules/.bin/jsdoc routes/categories.js 
