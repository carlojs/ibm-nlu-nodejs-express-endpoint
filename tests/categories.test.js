import { expect } from "chai"
import httpMocks from "node-mocks-http"
import { pullCategories } from "../routes/categories"


describe("Categories route", () => {
    describe("pullCategories function", () => {
        it("should return an object as response", () => {
            const req = httpMocks.createRequest({
                params: {
                    url: 'www.google.com'
                }
            });
            const res = httpMocks.createResponse();
            return pullCategories(req, res).then(() => {
                expect(JSON.parse(res._getData())).to.be.a('object');
            });
        })
        it("which contains an array of categories if a valid url is provided", () => {
            const req = httpMocks.createRequest({
                params: {
                    url: 'www.google.com'
                }
            });
            const res = httpMocks.createResponse();
            return pullCategories(req, res).then(() => {
                expect(JSON.parse(res._getData()).categories).to.be.a('array');
            });
        })
        it("or an error message if an invalid url is provided", () => {
            const req = httpMocks.createRequest({
                params: {
                    url: 'qwerty'
                }
            });
            const res = httpMocks.createResponse();
            return pullCategories(req, res).then(() => {
                expect(JSON.parse(res._getData()).error).to.be.a('string');
            });
        })
    })
})
