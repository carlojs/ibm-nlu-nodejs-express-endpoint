import express from "express";
import cors from "cors";
import { pullCategories } from './routes/categories';

const app = express()

// Allows cross-origin resource sharing
app.use(cors());

// API endpoint
app.get("/api/watson-nlu/:url", pullCategories)

// Server listens on port 3005
app.listen(3005)
