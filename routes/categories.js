import request from "request";

/**
 * Access IBM Watson API NLU endpoint and returns an object with categories
 * @param {object} req - Express request object, containing the url as param
 * @param {object} res - Express response object
 * @return {object} List of categories or error obj
 */
export const pullCategories = (req, res) => {
    return new Promise((resolve, reject) => {
        request.post("https://apikey:WKDN2Z1NBRSTiN5ewc4jVPaVrIvEZqOG-zrOX5e3Xbgt@gateway-lon.watsonplatform.net/natural-language-understanding/api/v1/analyze", {
            "headers": {
                "content-type": "application/json"
            },
            "json": {
                "url": req.params.url,
                "version": "2018-03-19",
                "features": {
                    "categories": {
                        "limit": 5
                    }
                }
            }
        }, (err, response, body) => {
            if (!err) {
                resolve(res.json(body));
            } else {
                reject(err);
            }
        })
    }).catch((err) => {
        console.log(err.message);
    });
}
